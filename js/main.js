
var NAMESPACE = NAMESPACE || {};
NAMESPACE.Main = function(){
    // =================================================
    // = Private variables (example: var _foo = bar; ) =
    // =================================================

    // =================================================
    // = public functions                              =
    // =================================================
    var self = {

        init: function()
        {
        	console.log('test');
        	//==========================
        	// init slidesjs
        	//==========================
			$(function(){
				$("#slides").slidesjs({
					width: 1024,
					height: 528,
					navigation: {
      					active: false,
					},
					play: {
				      effect: "slide",
				        // [string] Can be either "slide" or "fade".
				      interval: 3000,
				        // [number] Time spent on each slide in milliseconds.
				      auto: true,
				    }
				});
			});
			// animate content div on page load
			$( document ).ready(function() {
			    var $content = $('.content');
			    var height = $(window).height();
			    $content.css('top', height);
			    TweenLite.to($content, 1, {top:0, ease:Power2.easeOut});
			});

			// sticky nav
			var msie6 = $.browser == 'msie' && $.browser.version < 7;
			if (!msie6) {
			    var top = $('.nav ul').offset().top - parseFloat($('.nav').css('margin-top').replace(/auto/, 0));
			    console.log(top);
			    var _height = $('.nav ul').height();
			    $(window).scroll(function(event) {
			        var y = $(this).scrollTop();
			        var z = $('.footer').offset().top;
			        if (y >= top + 0 && (y+_height) < z) {
			            $('.nav ul').addClass('fixed');
			        } else {
			            $('.nav ul').removeClass('fixed');
			        }
			    });
			}
        }
    };

    return self;
    // ================================================
    // = Private functions (function _private () {} ) =
    // ================================================

}();

$(document).ready(NAMESPACE.Main.init);